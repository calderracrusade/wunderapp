#ifndef ForecastObj_h
#define ForecastObj_h
#include <ArduinoJson.h>

/*
 * Variables to store weather data
 * Anything not used should be commented out to save mem/processing
*/
class ForecastObj
{

	private:
			const char* api_error;

			const char* period1_icon;
			const char* period1_temp_hi;
			const char* period1_tmp_low;
			const char* period1_weekday_short;
			const char* period1_day;

			const char* period2_icon;
			const char* period2_temp_hi;
			const char* period2_tmp_low;
			const char* period2_weekday_short;
			const char* period2_day;

			const char* period3_icon;
			const char* period3_temp_hi;
			const char* period3_tmp_low;
			const char* period3_weekday_short;
			const char* period3_day;

			bool goodObject;


	public:
			ForecastObj(String*);
			~ForecastObj();

			int getPeriod1_icon();
			String getPeriod1_temp_hi();
			String getPeriod1_tmp_low();
			String getPeriod1_weekday_short();
			String getPeriod1_day();

			int getPeriod2_icon();
			String getPeriod2_temp_hi();
			String getPeriod2_tmp_low();
			String getPeriod2_weekday_short();
			String getPeriod2_day();

			int getPeriod3_icon();
			String getPeriod3_temp_hi();
			String getPeriod3_tmp_low();
			String getPeriod3_weekday_short();
			String getPeriod3_day();

			bool success();
};

#endif