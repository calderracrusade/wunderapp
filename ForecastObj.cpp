#include "ForeCastObj.h"
#include <ArduinoJson.h>
#include <Arduino.h>
ForecastObj::ForecastObj(String* forecastString)
{

	// DynamicJsonBuffer is not recommended for embedded systems.
	// I had some issues with exceptions using the StaticJsonBuffer, so I did use the dynamic buffer.
	// Should probably check into it again to see if I can get StaticJsonBuffer working.
	// If you do use StaticJsonBuffer, be sure you allocate enough memory for the json returned from api.
	Serial.println("Creating ForecastObj...");
	DynamicJsonBuffer jsonBuffer;

	// Create root object and parse the json file returned from the api.
	// The API does return errors, and if you do not check for them or for successful
	// object creation, the board will throw an exception.
	JsonObject& root = jsonBuffer.parseObject(*(forecastString));
	if(root.success())
	{
		Serial.println("ForecastObj root object created...");

		Serial.println("Begin serialization");
		JsonObject& forecast = root["forecast"];

		if(root.success())
		{

			goodObject = true;
			JsonArray& forecastdayArray = forecast["simpleforecast"]["forecastday"];

			// Day 1
			// root.forecast.simpleforecast.forecastday[0].icon
			JsonObject& day0 = forecastdayArray[0];
			period1_icon = day0["icon"];
			// root.forecast.simpleforecast.forecastday[0].high.fahrenheit
			period1_temp_hi = day0["high"]["fahrenheit"];
			// root.forecast.simpleforecast.forecastday[0].low.fahrenheit
			period1_tmp_low = day0["low"]["fahrenheit"];
			// root.forecast.simpleforecast.forecastday[0].date.weekday_short
			period1_weekday_short = day0["date"]["weekday_short"];
			// root.forecast.simpleforecast.forecastday[0].date.day
			period1_day = day0["date"]["day"];

			// Day 2
			JsonObject& day1 = forecastdayArray[1];
			period2_icon = day1["icon"];
			period2_temp_hi = day1["high"]["fahrenheit"];
			period2_tmp_low = day1["low"]["fahrenheit"];
			period2_weekday_short = day1["date"]["weekday_short"];
			period2_day = day1["date"]["day"];

			// Day 3
			JsonObject& day2 = forecastdayArray[2];
			period3_icon = day2["icon"];
			period3_temp_hi = day2["high"]["fahrenheit"];
			period3_tmp_low = day2["low"]["fahrenheit"];
			period3_weekday_short = day2["date"]["weekday_short"];
			period3_day = day2["date"]["day"];

		}
		else
		{
			// not able to create the object, so mark it as no good, and start over with a
			// fresh call to the API after 60 seconds.
			goodObject = false;
			Serial.println("Unable to create future_observation object.");
		}
	}
	else
	{
		// not able to create the object, so mark it as no good, and start over with a
		// fresh call to the API after 60 seconds.
		goodObject = false;
		Serial.println("Unable to create root object.");
	}

}

ForecastObj::~ForecastObj()
{
	// Just making sure the object is being destroyed.
	Serial.println("Object destroyed...");
}

// Getter functions for the saved weather data.
int ForecastObj::getPeriod1_icon()
{
	Serial.println("getPeriod1_icon");
	Serial.println(period1_icon);

	if(strcmp(period1_icon,"clear") == 0)
		return 1;

	if(strcmp(period1_icon, "cloudy") == 0)
		return 2;

	if(strcmp(period1_icon, "mostlycloudy") == 0)
		return 3;

	if(strcmp(period1_icon, "rain") == 0)
		return 4;

	if(strcmp(period1_icon, "sunny") == 0)
		return 5;

	if(strcmp(period1_icon, "tstorms") == 0)
		return 6;

	if(strcmp(period1_icon, "partlycloudy") == 0)
		return 7;

	if(strcmp(period1_icon, "snow") == 0)
		return 8;

	return 5;
}

String ForecastObj::getPeriod1_temp_hi()
{
	return period1_temp_hi;
}

String ForecastObj::getPeriod1_tmp_low()
{
	return period1_tmp_low;
}

String ForecastObj::getPeriod1_weekday_short()
{
	return period1_weekday_short;
}

String ForecastObj::getPeriod1_day()
{
	return period1_day;
}

int ForecastObj::getPeriod2_icon()
{
	Serial.println("getPeriod2_icon");
	Serial.println(period2_icon);

	if(strcmp(period2_icon,"clear") == 0)
		return 1;

	if(strcmp(period2_icon, "cloudy") == 0)
		return 2;

	if(strcmp(period2_icon, "mostlycloudy") == 0)
		return 3;

	if(strcmp(period2_icon, "rain") == 0)
		return 4;

	if(strcmp(period2_icon, "sunny") == 0)
		return 5;

	if(strcmp(period2_icon, "tstorms") == 0)
		return 6;

	if(strcmp(period2_icon, "partlycloudy") == 0)
		return 7;

	if(strcmp(period2_icon, "snow") == 0)
		return 8;

	return 5;
}

String ForecastObj::getPeriod2_temp_hi()
{
	return period2_temp_hi;
}

String ForecastObj::getPeriod2_tmp_low()
{
	return period2_tmp_low;
}

String ForecastObj::getPeriod2_weekday_short()
{
	return period2_weekday_short;
}

String ForecastObj::getPeriod2_day()
{
	return period2_day;
}

int ForecastObj::getPeriod3_icon()
{

	Serial.println("getPeriod3_icon");
	Serial.println(period3_icon);

	if(strcmp(period3_icon,"clear") == 0)
		return 1;

	if(strcmp(period3_icon, "cloudy") == 0)
		return 2;

	if(strcmp(period3_icon, "mostlycloudy") == 0)
		return 3;

	if(strcmp(period3_icon, "rain") == 0)
		return 4;

	if(strcmp(period3_icon, "sunny") == 0)
		return 5;

	if(strcmp(period3_icon, "tstorms") == 0)
		return 6;

	if(strcmp(period3_icon, "partlycloudy") == 0)
		return 7;

	if(strcmp(period3_icon, "snow") == 0)
		return 8;

	return 5;
}

String ForecastObj::getPeriod3_temp_hi()
{
	return period3_temp_hi;
}

String ForecastObj::getPeriod3_tmp_low()
{
	return period3_tmp_low;
}

String ForecastObj::getPeriod3_weekday_short()
{
	return period3_weekday_short;
}

String ForecastObj::getPeriod3_day()
{
	return period3_day;
}

bool ForecastObj::success()
{
	if(goodObject)
		return true;
	else
		return false;
}
