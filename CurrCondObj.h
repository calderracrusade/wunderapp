#ifndef CurrCondObj_h
#define CurrCondObj_h
#include <ArduinoJson.h>

/*
 * Variables to store weather data
 * Anything not used should be commented out to save mem/processing
*/
class CurrCondObj
{
	
	private:
			const char* api_error;
//			const char* current_observation_display_location_full;
//			const char* current_observation_display_location_city;
//			const char* current_observation_display_location_state;
//			const char* current_observation_display_location_zip;
//			const char* current_observation_display_location_country;
//			const char* current_observation_local_time_rfc822;
			const char* current_observation_observation_time;
			const char* current_observation_weather;
			const char* current_observation_temperature_string;
			const char* current_observation_temp_f;
			const char* current_observation_temp_c;
//			const char* current_observation_relative_humidity;
			const char* current_observation_wind_string;
			const char* current_observation_wind_mph;
			const char* current_observation_wind_dir;
//			const char* current_observation_precip_today_string;
//			const char* current_observation_precip_today_in;
//			const char* current_observation_precip_today_metric;
			const char* current_observation_icon;
			bool goodObject;


	public:
			CurrCondObj(String*);
			~CurrCondObj();
//			String getCityState();
//			String getCity();
//			String getState();
//			String getZip();
//			String getCountry();
//			String getLocalTime();
			String getObsTime();
			String getCurrWeather();
			String getCurrTempString();
			String getCurrF();
			String getCurrC();
//			String getRelHum();
			String getWindString();
			String getWindMPH();
			String getWindDir();
//			String getPrecipTodayString();
//			String getPrecipTodayInches();
//			String getPrecipTodayMet();
			int getWeatherIcon();
			bool success();
};

#endif