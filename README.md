# README #


```
#!cpp

/**********************************************************************
 * wunder_app.cpp
 * A Weather Widget App for ESP32
 * Author: reddit.com/user/Calderracrusade/
 * 
 * Based on hardware from Hackerboxes #0015
 * instructables.com/id/HackerBoxes-0015-Connect-Everything/?ALLSTEPS
 * Base unit: ESP32 - Developed/tested on Devkit C rev.2
 * OLED Display: SSD1306 mono 128x64 I2C w/ 0.9 diagonal
 * 
 * Current features Mar 5, 2017:
 *  -Connects to WiFi
 *  -Downloads weather from WUnderground API (free version)
 *  -Current weather w/ icon, temp, winds, time of observation
 *  -Second screen w/ 3-day forecast, icon, lo/hi temps
 *  -Configurable polling intervals to control data/power
 * 
 * Credit goes to jasper_fracture, this code was forked off of:
 * jasperfracture.com/basic-weather-widget-for-ssd1306-and-esp32/
 * 
 * Updates include:
 *  -Added a second page for 3-day forecast w/ switching display
 *  -Poll for updates 1/hour
 *  -General code cleanup, better var customization
 *  
 *  Notes for building:
 *   -If any dependencies complain about ESP8266WiFi, 
 *      -Edit to use WiFi.h from esp32 code instead
 *   -Currently uses DynamicJsonBuffer to avoid bugs
 *      -Ideally need to move to StaticJsonBuffer for lower mem usage
 *  
 **********************************************************************/
```