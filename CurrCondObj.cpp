#include "CurrCondObj.h"
#include <ArduinoJson.h>
#include <Arduino.h>
CurrCondObj::CurrCondObj(String* currCondString)
{

	// Any variables not currently in use have been commented out to save on processing/mem
	// If you want to use any commented vars, remember to also edit the .h file too

	// DynamicJsonBuffer is not recommended for embedded systems.
	// I had some issues with exceptions using the StaticJsonBuffer, so I did use the dynamic buffer.
	// Should probably check into it again to see if I can get StaticJsonBuffer working.
	// If you do use StaticJsonBuffer, be sure you allocate enough memory for the json returned from api.
	Serial.println("Creating CurrCondObj...");
	DynamicJsonBuffer jsonBuffer;

	// Create root object and parse the json file returned from the api.
	// The API does return errors, and if you do not check for them or for successful
	// object creation, the board will throw an exception.
	JsonObject& root = jsonBuffer.parseObject(*(currCondString));
	if(root.success())
	{
		Serial.println("CurrCondObj root object created...");
		JsonObject& current_observation = root["current_observation"];

		if(current_observation.success())
		{

			Serial.println("current_observation object created...");
			goodObject = true;

			// Store the parsed data in the appropriate object members.
			// The comments after the data assigmnments is what the returned data looks like.
//			current_observation_local_time_rfc822 = current_observation["local_time_rfc822"]; // "Mon, 06 Feb 2017 15:59:51 -050
			current_observation_observation_time = current_observation["observation_time"]; // "Last Updated on February 6, 3:59 PM EST"

			current_observation_weather = current_observation["weather"]; // "Overcast"
//			current_observation_temperature_string = current_observation["temperature_string"]; // "51.3 F (10.7 C)"
			current_observation_temp_f = current_observation["temp_f"]; // 51.3
			current_observation_temp_c = current_observation["temp_c"]; // 10.7
//			current_observation_relative_humidity = current_observation["relative_humidity"]; // "98%"
//			current_observation_wind_string = current_observation["wind_string"]; // "Calm"
			current_observation_wind_mph = current_observation["wind_mph"]; // 0
			current_observation_wind_dir = current_observation["wind_dir"]; // "ESE"
//			current_observation_precip_today_string = current_observation["precip_today_string"]; // "0.20 in (5 mm)"
//			current_observation_precip_today_in = current_observation["precip_today_in"]; // "0.20"
//			current_observation_precip_today_metric = current_observation["precip_today_metric"]; // "5"
			current_observation_icon = current_observation["icon"]; // "cloudy"

		}
		else
		{
			// not able to create the object, so mark it as no good, and start over with a 
			// fresh call to the API after 60 seconds.
			goodObject = false;
			Serial.println("Unable to create current_observation object.");
		}
	}
	else
	{
		// not able to create the object, so mark it as no good, and start over with a 
		// fresh call to the API after 60 seconds.
		goodObject = false;
		Serial.println("Unable to create root object.");	
	}

}

CurrCondObj::~CurrCondObj()
{
	// Just making sure the object is being destroyed.
	Serial.println("Object destroyed...");
}

//String CurrCondObj::getLocalTime()
//{
//	return current_observation_local_time_rfc822;
//}

String CurrCondObj::getObsTime()
{
	return current_observation_observation_time;
}

String CurrCondObj::getCurrWeather()
{
	return current_observation_weather;
}

//String CurrCondObj::getCurrTempString()
//{
//	return current_observation_temperature_string;
//}

String CurrCondObj::getCurrF()
{
	return current_observation_temp_f;
}

String CurrCondObj::getCurrC()
{
	return current_observation_temp_c;
}

//String CurrCondObj::getRelHum()
//{
//	return current_observation_relative_humidity;
//}
//
//String CurrCondObj::getWindString()
//{
//	return current_observation_wind_string;
//}

String CurrCondObj::getWindMPH()
{
	return current_observation_wind_mph;
}

String CurrCondObj::getWindDir()
{
	return current_observation_wind_dir;
}

//String CurrCondObj::getPrecipTodayString()
//{
//	return current_observation_precip_today_string;
//}
//
//String CurrCondObj::getPrecipTodayInches()
//{
//	return current_observation_precip_today_in;
//}
//
//String CurrCondObj::getPrecipTodayMet()
//{
//	return current_observation_precip_today_metric;
//}

int CurrCondObj::getWeatherIcon()
{
	if(strcmp(current_observation_icon,"clear") == 0)
		return 1;

	if(strcmp(current_observation_icon, "cloudy") == 0)
		return 2;

	if(strcmp(current_observation_icon, "mostlycloudy") == 0)
		return 3;

	if(strcmp(current_observation_icon, "rain") == 0)
		return 4;

	if(strcmp(current_observation_icon, "sunny") == 0)
		return 5;

	if(strcmp(current_observation_icon, "tstorms") == 0)
		return 6;

	if(strcmp(current_observation_icon, "partlycloudy") == 0)
		return 7;

	if(strcmp(current_observation_icon, "snow") == 0)
		return 8;

	return 5;
}

bool CurrCondObj::success()
{
	if(goodObject)
		return true;
	else 
		return false;
}